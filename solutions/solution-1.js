/*
Создайте в файле переменную message и запишите в нее строку “First Node.js file create!”
Напишите код, который будет выводить в консоль переменную message.
Выполните этот файл в терминале с помощью Node.js.
*/

const message = "First Node.js file create!"; // создаем переменную как и в обычном браузерном JavaScript
console.log(message); // выводим ее в консоль

// в терминале пишем node solution-1
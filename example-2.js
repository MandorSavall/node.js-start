const fs = require("fs");

fs.truncate('example.txt', 0, function(){
    console.log('Clear file content');
})

const content = "Node.js can change file content";

fs.writeFile("example.txt", content, (err)=> {
    if(!err){
        console.log("File change success");
    }
});
